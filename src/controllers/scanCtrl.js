var fs = require('fs');
var path = require('path');
var mm = require("musicmetadata")
var series = require("async/series");
var parallelLimit = require("async/parallelLimit")
var series = require("async/series")

var walk = function (dir, done) {

    var results = [];
    var funcs = [];

    fs.readdir(dir, function (err, list) {

        if (err) return done(err);

        var pending = list.length;
        if (!pending) return done(null, results);

        list.forEach(function (file) {

            file = path.resolve(dir, file);

            fs.stat(file, function (err, stat) {

                if (stat && stat.isDirectory()) {

                    walk(file, function (err, res) {
                        results = results.concat(
                            res);
                        if (!--pending) done(
                            null,
                            results);
                    });
                } else {

                    if (file.indexOf(".mp3") !== -1
                        || file.indexOf(".m4a") !== -1
                        || file.indexOf(".ogg") !== -1
                        || file.indexOf(".flac") !== -1
                        || file.indexOf(".wma") !== -1
                        || file.indexOf(".wmv") !== -1
                        || file.indexOf(".mp4") !== -1
                        || file.indexOf(".mkv") !== -1
                        || file.indexOf(".avi") !== -1) {

                        results.push(file);
                        if (!--pending) done(null, results);
                    } else {
                        if (!--pending) done(null, results);
                        return;
                    }
                }
            });
        });
    });
};

var getMetaDataFunc = function (file) {

    //console.log("getMetaData function called");
    //console.log(file);

    return function (callback) {

        if (file.indexOf(".mp3") !== -1
            || file.indexOf(".m4a") !== -1
            || file.indexOf(".ogg") !== -1
            || file.indexOf(".flac") !== -1
            || file.indexOf(".wma") !== -1
            || file.indexOf(".wmv") !== -1) {

            console.log("reading file meta data");
            console.log(file);

            var readableStream = fs.createReadStream(file)

            console.log("get's here!", file)

            var parser = mm(readableStream, {picture: false}, function (err, metadata) {

                console.log("now get's here", file);

                if (err) {

                    console.log("now now now get's here");

                    if (err.message === "Could not find metadata header"){

                        readableStream.close();
                        callback(null,{ filePath : file});

                    } else {

                        console.log("Error in reading meta data from file");
                        console.log(file);

                        readableStream.close();
                        callback(err);
                    }
                } else {

                    console.log("well!!! now get's here");

                    delete metadata.picture;

                    metadata.filePath = file;


                    readableStream.close();
                    readableStream.destroy();
                    callback(null, metadata);

                    console.log("really shouldn't get here");
                }

            });
        } else {
            callback(null);
        }
    }
}

var getWalkFunc = function(rootDir){

    return function(callback){

        walk(rootDir, function(err, results){
            callback(err, results)
        });
    }
}

var processVideoData = function(videos){

    var arrLen = videos.length;
    var ret = [];

    for (var i = 0; i < arrLen; i++){
        if (videos[i].indexOf(".wmv") > -1
            || videos[i].indexOf(".avi") > -1
            || videos[i].indexOf(".mkv") > -1
            || videos[i].indexOf(".mp4")  > -1){

            var path = videos[i].match(/[^\\]*\.(\w+)$/);

            ret.push({ filePath : videos[i], name : path[0]})
        }
    }

    return ret;
}

module.exports = function (config) {

    var configJson = JSON.parse(fs.readFileSync(path.resolve(__dirname, config)));

    return function (req, res) {

        var metaDataFuncs = [];
        var mp3DirFuncs = [];
        var videoDirFuncs = [];

        //scan all config folders and return json to client
        for (var item in configJson) {

            if (item === "mp3") {

                for (var i = 0; i < configJson[item].length; i++) {
                    mp3DirFuncs.push(getWalkFunc(configJson[item][i]));
                }
            } else if (item === "video"){

                for (var i = 0; i < configJson[item].length; i++){
                   videoDirFuncs.push(getWalkFunc(configJson[item][i]))
                }
            }
        }

        series(mp3DirFuncs, function(err, results){

            if (err){

                console.log("Error reading mp3 directories");
                console.log(err);

                res.status(500);
                res.json({
                    message: "Error reading mp3 directories"
                });
            } else {

                var mp3s = [];

                for (var i = 0; i < results.length; i++){
                    mp3s = mp3s.concat(results[i]);
                }

                var arrLen = mp3s.length;

                for (var i = 0; i < arrLen; i++) {
                    metaDataFuncs.push(getMetaDataFunc(mp3s[i]));
                }

                series(videoDirFuncs, function(err, results){

                    if (err){

                        console.log("Error reading video directories");
                        console.log(err);

                        res.status(500);
                        res.json({
                            message: "Error reading video directories"
                        });
                    } else {


                        var videos = [];

                        for (var i = 0; i < results.length; i++){
                            videos =  videos.concat(processVideoData(results[i]));
                        }

                        parallelLimit(metaDataFuncs, 1, function(err, metadata){
                            //series(funcs, function (err, metadata) {

                            if (err) {
                                console.log("Error reading mp3 metadata");
                                console.log(err);

                                res.status(500);
                                res.json({
                                    message: "Error reading video directories"
                                });
                            } else {
                                res.json({mp3s : metadata, videos : videos});
                            }
                        })
                    }
                });
            }
        });
    }
}