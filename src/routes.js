var scanCtrl = require("./controllers/scanCtrl");

module.exports = function(app, config){

  app.get("/api/scan/", scanCtrl(config))
};
