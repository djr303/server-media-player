var express = require('express');
var cors = require("cors");

var app = express();
var routes = require("./routes");

var args = [];

process.argv.forEach(function (val, index, array) {
  args[index] = val;
});

app.use(cors());

routes(app, args[2].split("=")[1]);

var server = app.listen(3001, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Media player server listening at http://%s:%s', host, port);
});

server.on('connection', function(socket) {
  console.log("A new connection was made by a client.");
  socket.setTimeout(6000 * 1000);
})
